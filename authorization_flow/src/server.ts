import dotenv from 'dotenv';
import express from 'express';
import initRoutes from './routes';

dotenv.config();

const l = console.log;

const port = process.env.port ? parseInt(process.env.port) : 3000;

const publicPath = `${__dirname}/../public`;

const app = express();
app.set('view engine', 'ejs')
  .set('views', publicPath);

app.get('/ok', (req, resp) => {
  resp.send('ok');
});

initRoutes(app);

app.use(express.static(publicPath));
app.listen(port, () => {
  l(`Server on port ${port}`);
});
