import { readFile } from 'fs';
import { join } from 'path';

import { Response } from 'express';

export const sendTemplate = (response: Response, name: string, params: any = null ) => {
  const templatePath = join(__dirname, 'public', name);
  readFile(templatePath, 'utf8', (err: NodeJS.ErrnoException | null, contents: string)  => {
    if (err) {
      response
        .status(500)
        .send(`template not found ${templatePath}`);
    } else {
      let out = contents;
      if (params && typeof params === 'object') {
          Object.entries(params).forEach(([k, v]) => {
            let text = '';
            if (typeof v === 'object') text = JSON.stringify(v, null, 2);
            else text = (v as object).toString();
            out = out.replace(`@@${k}`, text)
            console.log('replacing ', `@@${k}` , text);
          });
      }
      response.send(out);
    }
  });
};

export const composeUrl = (baseUrl: string, params: { [id: string]: string }) : string => {
  let url = baseUrl;
  
  if (baseUrl.indexOf('?') < 0) {
    url += '?';
  } else if(baseUrl[baseUrl.length -1] !== '&') {
    url += '&';
  }

  Object.entries(params).forEach(([k, v]) => {
    url = `${url}${k}=${encodeURIComponent(v)}&`;
  });

  return url;
};

interface DecodeJwt {
  header: { [id: string]: any },
  payload: { [id: string]: any },
}

const decodeBase64json = (base64: string) => {
  return JSON.parse(Buffer.from(base64, 'base64').toString('utf8'));
}

export const decomposeJwt = (jwt: string): DecodeJwt | undefined => {
  const [eHeader, ePayload] = jwt.split('.');
  return {
    header: decodeBase64json(eHeader),
    payload: decodeBase64json(ePayload),
  };
};
 
