import { Application } from 'express';
import axios from 'axios';
import { composeUrl, decomposeJwt } from './utils';


let database : {[id: string]: any} = {};


const initRoutes = (app: Application) : void => {

  const applicationId = process.env['APPLICATION_ID'] as string;
  const applicationSecret = process.env['APPLICATION_SECRET'] as string;
  const endpointAuth =  process.env['ENDPOINT_AUTH'] as string;
  const endpointToken = process.env['ENDPOINT_TOKEN'] as string;
  const endpointUserInfo = process.env['ENDPOINT_USERINFO'] as string;
  const redirectUrl = process.env['REDIRECT_URL'] as string;

  app.get('/', (req, res) => {
    res.render('index.ejs', {
      data: database,
    }) ;
  });

  app.get('/clear', (req, res) => {
    database = {};
    res.redirect('/');
  });

  app.get('/auth/perform-login', (req, res) => {

    const authParams = {
      'client_id': applicationId,
      'response_type': 'code',     // flow type
      'redirect_uri': redirectUrl, // url to send the response to
      'scope': 'openid profile email', // list of scopes
      'access_type': 'offline', // for refresh token, some providers use scope=offline_access
      //'scope': 'openid profile email https://www.googleapis.com/auth/calendar.events.readonly',
      'nonce': 'aaasss', // should be random
    };

    const authUrl = composeUrl(endpointAuth, authParams);


    database = { authUrl, authParams };

    console.log('auth url',authUrl);

    res.redirect(authUrl);
  });

  app.get('/auth/redirect', async (req, res) => {
    try {
      const { code } = req.query;
      database.auth_response= req.query;

      // complete authorization_code flow
      const authCodeParams = {
        'client_id': applicationId,
        'client_secret': applicationSecret,
        'grant_type': 'authorization_code',
        'redirect_uri': redirectUrl,
        'code': code,
      };

      database.token_params = authCodeParams;

      const response = await axios.post(endpointToken, authCodeParams );
      
      database.token_response= response.data;

      const {
        access_token: accessToken,
        id_token: idToken,
      } = response.data;

      const jwt = decomposeJwt(idToken);
      database.jwt = jwt;

      // call user info 
      const userInfoUrl = composeUrl(endpointUserInfo, {
        'access_token': accessToken,
      });
      
      const userInfoResponse = await axios.get(userInfoUrl);
      database.user_info= userInfoResponse.data;
      
    } catch (err) {
      console.warn(err);
      database.error = err;
    }
    res.redirect('/');
  });

};

export default initRoutes;

