import dotenv from 'dotenv';
import express from 'express';

dotenv.config();

const l = console.log;

const port = process.env.port ? parseInt(process.env.port) : 3001;

const publicPath = `${__dirname}/../public`;

const app = express();
app.set('view engine', 'ejs')
  .set('views', publicPath);

app.get('/ok', (req, resp) => {
  resp.send('ok');
});

app.get('/', (req, res) => {
  res.render('index', {
    env: process.env,
  });
});

app.use(express.static(publicPath));

app.listen(port, () => {
  l(`Server on port ${port}`);
});
